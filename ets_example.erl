%%%-------------------------------------------------------------------
%%% @author Piotr Wójcik
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. sty 2015 09:38
%%%-------------------------------------------------------------------
-module(ets_example).
-author("Piotr Wójcik").

%% API
-export([start_insert/1, start_insert_new/1, process/1, read/1, write/1, test/1]).

start_insert(Type) ->
  Tid = ets:new(table, Type),
  ets:insert(Tid, {1,2,3}),
  ets:insert(Tid, {"foo", "bar", 2}),
  ets:insert(Tid, {key, 3, 23}),
  ets:insert(Tid, {"foo", "bar", 6}),
  ets:insert(Tid, {key, 3, 23}),
  ets:insert(Tid, {key, 3, 26}),
  ets:insert(Tid, {key, 3, 23}),
  Tid.


start_insert_new(Type) ->
  Tid = ets:new(table, Type),
  ets:insert_new(Tid, {1,2,3}),
  ets:insert_new(Tid, {"foo", "bar", 2}),
  ets:insert_new(Tid, {key, 3, 23}),
  ets:insert_new(Tid, {"foo", "bar", 6}),
  ets:insert_new(Tid, {key, 3, 23}),
  ets:insert_new(Tid, {key, 3, 26}),
  ets:insert_new(Tid, {key, 3, 23}),
  Tid.

read(Msg) ->
  b ! {read, self(), Msg},
  receive
    _ -> io:format("Read OK~n")
  end.

process(Tid) ->
  receive
    {read, Pid,  A} -> ets:lookup(Tid, A), Pid ! ok;
    {write, Pid, A} -> ets:insert_new(Tid, A), Pid ! ok
  end.

write(Msg) ->
  b ! {write, self(), Msg},
  receive
    _ -> io:format("Write OK~n")
  end.


test(Access) ->
  Tid = start_insert([bag, Access]),
  register(b, spawn(?MODULE, process, [Tid])),
  read(key),
  register(b, spawn(?MODULE, process, [Tid])),
  write({element, wlozony, przez, watek, b}),
  ok.
